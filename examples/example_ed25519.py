from thcrypto import Ed25519


# Generate Ed25519
generated_ed25519: Ed25519 = Ed25519.generate().unwrap()


# Load ed25519 keys from files - if files not exists then generates Ed25519,
# writes private and public keys to the files and returns Ed25519
# Parameters (private_path: str='custom_private_path.pem', public_path: str='custom_public_path.pem') are optional,
# if not passed then default params are used
# (private_path: str='sk_ed25519.pem', public_path: str='pk_ed25519.pem')
ed25519_: Ed25519 = Ed25519.load_or_generate_file().unwrap()


# Sign data with ed25519 private key
signed_data: bytes = ed25519_.sign(b'test_test_b').unwrap()


# Verify signature with ed25519 public key
verified: bool = ed25519_.verify(signed_data, b'test_test_b').unwrap()


# Get serialized bytes from the private key.
private_key_bytes: bytes = ed25519_.get_raw_private_key_bytes().unwrap()


# Get serialized bytes from the private key decoded to string
private_key_raw: bytes = ed25519_.get_raw_private_key().unwrap()


# Get serialized bytes from the public key.
raw_public_key_bytes: bytes = ed25519_.get_raw_public_key_bytes().unwrap()


# Get serialized bytes from the public key decoded to string
raw_public_key: bytes = ed25519_.get_raw_public_key().unwrap()
