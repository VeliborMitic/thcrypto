from time import time

from thcrypto import Fernet


# Generate Fernet
generated_fernet: Fernet = Fernet.generate().unwrap()


# Load Fernet from file or generate one
# This function tries to load the Fernet from file, if file does not exists then generate Fernet
# and writes it down to a file. Function accepts optional 'path' parameter (path: str='custom_path.key')
# or uses default value (path: str='fernet.key')
fernet_: Fernet = Fernet.load_or_generate_file().unwrap()


# Encrypt bytes with Fernet key
# This function takes required argument 'data_bytes' and optional argument 'current_time'
# (data_bytes: bytes, current_time: int | None=None)
encrypted_bytes: bytes = fernet_.encrypt_bytes(data_bytes=b'test bytes', current_time=int(time())).unwrap()


# Decrypt bytes with Fernet key
# This function takes required argument 'data_bytes' and two optional arguments 'ttl' and 'current_time'
# (enc_data_bytes: bytes, ttl: int | None=None, current_time: int | None=None)
decrypt_bytes: bytes = fernet_.decrypt_bytes(enc_data_bytes=encrypted_bytes, ttl=100, current_time=int(time())).unwrap()


# Encrypt dict with Fernet key
# This function takes required argument 'data_dict' and optional argument 'current_time'
# (data_dict: dict, current_time: int | None=None)
encrypted_dict: str = fernet_.encrypt_dict(data_dict={'test_key': 'test_value'}, current_time=int(time())).unwrap()


# Decrypt dict with Fernet key
# This function takes required argument 'enc_data_str' and two optional arguments 'ttl' and 'current_time'
# (enc_data_str: str, ttl: int | None=None, current_time: int | None=None)
decrypted_dict: dict = fernet_.decrypt_dict(enc_data_str=encrypted_dict, ttl=100, current_time=int(time())).unwrap()
