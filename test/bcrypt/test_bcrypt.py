import pytest

from thcrypto import Bcrypt


@pytest.fixture(scope='module')
def generated_bcrypt():
    bcrypt_generated: Bcrypt = Bcrypt.generate().unwrap()
    assert isinstance(bcrypt_generated, Bcrypt)

    return bcrypt_generated



def test_generate_default_params_success():
    """
    This function tests generating bcrypt salt without params (default params used) - success expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.generate().unwrap()
    assert isinstance(bcrypt_generated, Bcrypt)


def test_generate_custom_params_success():
    """
    This function tests generating bcrypt salt with custom params passed - success expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.generate(12, b'2b').unwrap()
    assert isinstance(bcrypt_generated, Bcrypt)


@pytest.mark.xfail(raises=TypeError, strict=True)
def test_generate_error():
    """
    This function tests generating bcrypt salt with wrong type param passed - TypeError expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.generate('12', b'2b').unwrap()


def test_load_or_generate_file_generate_case():
    """
    This function tests bcrypt salt load or generate when file dont exist.
    Function generates bcrypt key and writes it down to the file - success expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.load_or_generate_file().unwrap()
    assert isinstance(bcrypt_generated, Bcrypt)


def test_load_or_generate_file_load_case():
    """
    This function tests bcrypt salt load or generate. Fist function call generates bcrypt salt
    and writes it down to the file, while second function call loads bcrypt salt key from previously
    generated file - success expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.load_or_generate_file().unwrap()
    assert isinstance(bcrypt_generated, Bcrypt)

    bcrypt_loaded: Bcrypt = Bcrypt.load_or_generate_file().unwrap()
    assert isinstance(bcrypt_loaded, Bcrypt)


@pytest.mark.xfail(raises=TypeError, strict=True)
def test_load_or_generate_file_error():
    """
    This function tests load or generate bcrypt salt with wrong type param passed - TypeError expected
    """
    bcrypt_generated: Bcrypt = Bcrypt.load_or_generate_file('12', b'2b').unwrap()


def test_get_hashed_password(generated_bcrypt):
    """
    This function tests get hashed password using class function - success expected
    """
    # class method test
    hashed_password: bytes = Bcrypt.get_hashed_password(generated_bcrypt, 'test bcrypt').unwrap()
    assert isinstance(hashed_password, bytes)

    # instance method test
    hashed_password: bytes = generated_bcrypt.get_hashed_password('test bcrypt').unwrap()
    assert isinstance(hashed_password, bytes)


@pytest.mark.xfail(raises=ValueError, strict=True)
def test_get_hashed_password_error(generated_bcrypt):
    """
    This function tests get hashed password - maximum input length (72 bytes) exceeded - ValueError expected
    """
    plain_text_password_75_bytes: str = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry a'
    b: bytes = Bcrypt.get_hashed_password(generated_bcrypt, plain_text_password_75_bytes).unwrap()


def test_check_password(generated_bcrypt):
    """
    This function tests check password - success expected, returns True
    """
    plain_text_password: str = 'test bcrypt'

    # class method test
    hashed_password: bytes = generated_bcrypt.get_hashed_password(plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    result: bool = Bcrypt.check_password(plain_text_password, hashed_password).unwrap()
    assert result is True

    # instance method test
    hashed_password: bytes = generated_bcrypt.get_hashed_password(plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    result: bool = generated_bcrypt.check_password(plain_text_password, hashed_password).unwrap()
    assert result is True


def test_check_password_string_hashed_password_passed(generated_bcrypt):
    """
    This function tests check password - success expected, returns True
    """
    plain_text_password: str = 'test bcrypt'

    # class method test
    hashed_password: bytes = Bcrypt.get_hashed_password(generated_bcrypt, plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    string_hash_password: str = hashed_password.decode('utf-8')
    result: bool = Bcrypt.check_password(plain_text_password, string_hash_password).unwrap()
    assert isinstance(hashed_password, bytes)
    assert result is True

    # instance method test
    hashed_password: bytes = generated_bcrypt.get_hashed_password(plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    string_hash_password: str = hashed_password.decode('utf-8')
    result: bool = generated_bcrypt.check_password(plain_text_password, string_hash_password).unwrap()
    assert isinstance(hashed_password, bytes)
    assert result is True


def test_check_password_wrong_password(generated_bcrypt):
    """
    This function tests check password, when wrong password passed, returns False
    """
    plain_text_password: str = 'test bcrypt'

    # class method test
    hashed_password: bytes = Bcrypt.get_hashed_password(generated_bcrypt, plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    result: bool = Bcrypt.check_password('wrong password', hashed_password).unwrap()
    assert isinstance(hashed_password, bytes)
    assert result is False

    # instance method test
    hashed_password: bytes = generated_bcrypt.get_hashed_password(plain_text_password).unwrap()
    assert isinstance(hashed_password, bytes)

    result: bool = generated_bcrypt.check_password('wrong password', hashed_password).unwrap()
    assert isinstance(hashed_password, bytes)
    assert result is False
