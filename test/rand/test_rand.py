import pytest

from thcrypto import (
    gen_random_int,
    gen_random_int_hex,
    gen_random_int_hex_bytes,
    gen_random_int128,
    gen_random_int128_hex,
    gen_random_int128_hex_bytes,
    gen_random_int256,
    gen_random_int256_hex,
    gen_random_int256_hex_bytes
)


def test_gen_random_int():
    '''
    Test generate a random integer function, based on passed number of bits - success expected
    '''
    random_int: int = gen_random_int(256).unwrap()
    assert isinstance(random_int, int)


@pytest.mark.xfail(raises=ValueError, strict=True)
def test_gen_random_int_error():
    '''
    Test generate a random integer function when negative value param passed - ValueError expected
    '''
    random_int: int = gen_random_int(-256).unwrap()
    assert isinstance(random_int, int)


def test_gen_random_int_hex():
    '''
    Test generate random string based on hexadecimal representation of an random integer
    '''
    random_int_hex: str = gen_random_int_hex(256).unwrap()
    assert isinstance(random_int_hex, str)


def test_gen_random_int_hex_bytes():
    '''
    Test Generate random bytes function -  - success expected
    '''
    random_int_hex_bytes: bytes = gen_random_int_hex_bytes(128).unwrap()
    assert isinstance(random_int_hex_bytes, bytes)


def test_gen_random_int128():
    '''
    Test generate a random integer function - success expected
    '''
    random_int128: int = gen_random_int128().unwrap()
    assert isinstance(random_int128, int)


def test_gen_random_int128_hex():
    '''
    Test generate random string based on hexadecimal representation of an random integer
    '''
    random_int128_hex: str =  gen_random_int128_hex().unwrap()
    assert isinstance(random_int128_hex, str)


def test_gen_random_int128_hex_bytes():
    '''
    gTest Generate random bytes function -  - success expected
    '''
    random_int128_hex_bytes: bytes = gen_random_int128_hex_bytes().unwrap()
    assert isinstance(random_int128_hex_bytes, bytes)


def test_gen_random_int256():
    '''
    Test generate a random integer function - success expected
    '''
    random_int256: int = gen_random_int256().unwrap()
    assert isinstance(random_int256, int)


def test_gen_random_int256_hex():
    '''
    Test generate random string based on hexadecimal representation of an random integer
    '''
    random_int256_hex: str = gen_random_int256_hex().unwrap()
    assert isinstance(random_int256_hex, str)


def test_gen_random_int256_hex_bytes():
    '''
    gTest Generate random bytes function -  - success expected
    '''
    random_int256_hex_bytes: bytes = gen_random_int256_hex_bytes().unwrap()
    assert isinstance(random_int256_hex_bytes, bytes)
